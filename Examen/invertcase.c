#include <stdio.h>

void invertcase(char *s) {
    while (*s != '\0') {
        if (*s >= 'a' && *s <= 'z') {
            *s = *s - 32; // Convert lowercase to uppercase
        } else if (*s >= 'A' && *s <= 'Z') {
            *s = *s + 32; // Convert uppercase to lowercase
        }
        s++;
    }
}

int main() {
    char str[] = "tO BE oR NOT to Be !";
    
    printf("Original string: %s\n", str);
    invertcase(str);
    printf("Inverted case string: %s\n", str);
    
    return 0;
}
