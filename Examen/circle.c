#include <stdio.h>

#define PI 3.14159265359

// Fonction pour calculer le périmètre d'un cercle
float perimetre(float r) {
    return 2 * PI * r;
}

// Fonction pour calculer la surface d'un disque
float surface(float r) {
    return PI * r * r;
}

int main() {
    float rayon;
    
    // Demande à l'utilisateur de saisir le rayon du cercle
    printf("Entrez le rayon du cercle : ");
    scanf("%f", &rayon);
    
    // Appel des fonctions perimetre et surface
    printf("Le perimetre du cercle est : %.2f\n", perimetre(rayon));
    printf("La surface du disque est : %.2f\n", surface(rayon));
    
    return 0;
}
