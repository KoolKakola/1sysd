#include <stdio.h>

int factorial(int n) {
    if (n == 0 || n == 1) {
        return 1;
    } else {
        return n * factorial(n - 1);
    }
}

int main() {
    int n;
    printf("Entrez un nombre entier : ");
    scanf("%d", &n);
    printf("La factorielle de %d est : %d\n", n, factorial(n));
    return 0;
}
